From: Aymeric Agon-Rambosson <aymeric.agon@yandex.com>
Date: Sun, 08 Jan 2023 18:37:41 +0100
Subject: Debianise documentation
Forwarded: not-needed

--- a/README.org
+++ b/README.org
@@ -1,11 +1,5 @@
 #+TITLE: Gnuplot for Emacs
 
-#+html: <a href="https://www.gnu.org/software/emacs/"><img alt="GNU Emacs" src="https://github.com/minad/corfu/blob/screenshots/emacs.svg?raw=true"/></a>
-#+html: <a href="https://elpa.nongnu.org/nongnu/gnuplot.html"><img alt="NonGNU ELPA" src="https://elpa.nongnu.org/nongnu/gnuplot.svg"/></a>
-#+html: <a href="https://elpa.nongnu.org/nongnu-devel/gnuplot.html"><img alt="NonGNU-devel ELPA" src="https://elpa.nongnu.org/nongnu-devel/gnuplot.svg"/></a>
-#+html: <a href="https://melpa.org/#/gnuplot"><img alt="MELPA" src="https://melpa.org/packages/gnuplot-badge.svg"/></a>
-#+html: <a href="https://stable.melpa.org/#/gnuplot"><img alt="MELPA Stable" src="https://stable.melpa.org/packages/gnuplot-badge.svg"/></a>
-
 This package allows running [[http://www.gnuplot.info/][gnuplot]] files from within the [[https://www.gnu.org/software/emacs/][GNU Emacs]]
 editor. It features:
 
@@ -18,44 +12,7 @@ editor. It features:
 It is recommended to run =gnuplot-mode= on GNU Emacs 25 or above,
 using gnuplot version 5.0 or above.
 
-* Installation
-** Using MELPA
-
-The easiest way to install =gnuplot-mode= is to directly get it from
-[[http://melpa.milkbox.net][MELPA]]. After [[http://melpa.milkbox.net/#installing][configuring Emacs to use MELPA]], you should be able to
-install gnuplot-mode by typing
-
-: M-x install-package RET gnuplot RET
-
-or do =M-x list-packages= and search for =gnuplot= in the list.
-
-** Using =el-get=
-
-The [[https://github.com/dimitri/el-get.git][el-get]] package includes a gnuplot-mode recipe. So to install
-simply call
-
-: M-x el-get-install RET gnuplot-mode
-
-Alternatively, you can directly place the following in your init file so that
-=el-get= can install and load gnuplot-mode at Emacs start up:
-
-: (el-get 'sync 'gnuplot-mode)
-
-** From source
-
-After fetching the package's source from [[https://github.com/emacs-gnuplot/gnuplot][the homepage]], byte-compile
-the package's files using the =make= command and move the compiled
-=.elc= files to your chosen target directory.
-
 * Configuration
-** Load Path
-
-First of all, make sure that =gnuplot.el= is in your load-path (this is automatic if using a package helper like use-package). To do so manually, add the following snippet in your emacs configuration file
-
-#+begin_src emacs-lisp
-(add-to-list 'load-path "/path/to/gnuplot")
-#+end_src
-
 ** Info File
 
 The function =gnuplot-info-lookup-symbol= looks at the Gnuplot info
@@ -67,19 +24,9 @@ to put the =gnuplot.info= any place conv
 (add-to-list 'Info-default-directory-list "/path/to/info/file")
 #+end_src
 
-** Enable Mode
-
-You can automatically enable =gnuplot-mode= using the snippet below:
-
-#+begin_src emacs-lisp
-(autoload 'gnuplot-mode "gnuplot" "Gnuplot major mode" t)
-(autoload 'gnuplot-make-buffer "gnuplot" "open a buffer in gnuplot-mode" t)
-(setq auto-mode-alist (append '(("\\.gp$" . gnuplot-mode)) auto-mode-alist))
-#+end_src
-
 * Usage
 
-Apart from enabling =gnuplot-mode= automatically (see above), these
+Apart from enabling =gnuplot-mode= automatically, these
 two functions are useful for starting up gnuplot-mode:
 
 - =M-x gnuplot-mode= : start gnuplot-mode in the current buffer
@@ -159,38 +106,6 @@ with image support. Please report bugs.
 
 * FAQ / Remarks
 
-** Usage on Windows
-
-Multiple users have reported issues when trying to work with
-=gnuplot.el= on Windows. Most notably, the gnuplot process hangs after
-sending a first line of input (this is a common Emacs issue on
-Windows, see [[https://www.gnu.org/software/emacs/manual/html_mono/efaq-w32.html#Sub_002dprocesses][here]]).
-
-A partial workaround was to use =pgnuplot.exe= as the
-=gnuplot-program=. However, =pgnuplot.exe= is not included with
-gnuplot since version 5.0.
-
-You currently have two solutions:
-
-1. Experiment using the =gnuplot-program= and =gnuplot-program-args=
-   variables. For instance, setting
-
-   #+begin_src emacs-lisp
-(setq gnuplot-program "/path/to/cmdproxy.exe")
-(setq gnuplot-program-args "/C /path/to/gnuplot.exe")
-   #+end_src
-
-   has been reported to work (see [[https://github.com/emacs-gnuplot/gnuplot/pull/33/files][here]] for a reference).
-
-2. Use the simpler [[https://github.com/mkmcc/gnuplot-mode][gnuplot-mode]] package that sends the entire buffer
-   to gnuplot. Since no =comint= is involved, it should function
-   correctly, but you lose most features of the =gnuplot.el= package.
-   We would like to implement a send-buffer without comint as well
-   eventually.
-
-More information on =gnuplot.el= and Windows can be found on these threads: [[https://github.com/emacs-gnuplot/gnuplot/issues/15][1]], [[https://github.com/emacs-gnuplot/gnuplot/pull/33][2]]
-
-
 ** Pause Command
 
 Gnuplot's =pause -1= command, which waits for the user to press a key,
